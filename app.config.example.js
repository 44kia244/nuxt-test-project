export default {
  database: {
    user: process.env.APP_PG_USER || 'postgres',
    host: process.env.APP_PG_HOST || '127.0.0.1',
    database: process.env.APP_PG_DB || 'postgres',
    password: process.env.APP_PG_PASS || 'P@ssw0rd',
    port: process.env.APP_PG_PORT ? (parseInt(process.env.APP_PG_PORT) || 5432) : 5432
  },
  firebaseAdmin: {
    // TODO: Get Firebase Admin SDK Config from 'console.firebase.google.com', exclude 'credential'.
  },
  serviceAccount: {
    // Insert serviceAccount.json content here (replace this whole object)
  },
  firebaseClient: {
    // Insert firebase initializeApp config here (replace this whole object)
  }
}
