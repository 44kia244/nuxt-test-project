export default class Lab {
  constructor (labId = null, labName = null, tags = null, vmImage = null, instructions = null, score = null, hint = null, solutionId = null) {
    this.setLabId(labId)
    this.setLabName(labName)
    this.setTags(tags)
    this.setVmImage(vmImage)
    this.setInstructions(instructions)
    this.setScore(score)
    this.setHint(hint)
    this.setSolutionId(solutionId)
  }

  getLabId () {
    return this.labId
  }

  getLabName () {
    return this.labName
  }

  getTags () {
    return this.tags
  }

  getVmImage () {
    return this.vmImage
  }

  getInstructions () {
    return this.instructions
  }

  getScore () {
    return this.score
  }

  getHint () {
    return this.hint
  }

  getSolutionId () {
    return this.solutionId
  }

  setLabId (labId) {
    // string 20
    this.labId = labId
  }

  setLabName (labName) {
    // string 100
    this.labName = labName
  }

  setTags (tags) {
    // array<string 30>
    this.tags = tags
  }

  setVmImage (vmImage) {
    // string 150
    this.vmImage = vmImage
  }

  setInstructions (instructions) {
    // string unlimit
    this.instructions = instructions
  }

  setScore (score) {
    // number
    this.score = score
  }

  setHint (hint) {
    // string unlimit
    this.hint = hint
  }

  setSolutionId (solutionId) {
    // string 20
    this.solutionId = solutionId
  }

  toJSON () {
    return {
      labId: this.getLabId(),
      labName: this.getLabName(),
      tags: this.getTags(),
      vmImage: this.getVmImage(),
      instructions: this.getInstructions(),
      score: this.getScore(),
      hint: this.getHint(),
      solutionId: this.getSolutionId()
    }
  }

  validate () {
    if (typeof this.labId !== 'string') return false
    if (this.labName !== Symbol.for('unchanged') && (typeof this.labName !== 'string' || this.labName.length <= 0)) return false
    if (this.tags !== Symbol.for('unchanged') && (!Array.isArray(this.tags) || !this.tags.every(e => typeof e === 'string') || this.tags.length <= 0)) return false
    if (this.vmImage !== Symbol.for('unchanged') && (typeof this.vmImage !== 'string' || this.vmImage.length <= 0 || !this.vmImage.startsWith('gs://'))) return false
    if (this.instructions !== Symbol.for('unchanged') && (typeof this.instructions !== 'string' || this.instructions.length <= 0)) return false
    if (this.score !== Symbol.for('unchanged') && (typeof this.score !== 'number' || this.score <= 0)) return false
    if (this.hint !== Symbol.for('unchanged') && (typeof this.hint !== 'string' || this.hint.length <= 0)) return false
    if (this.solutionId !== Symbol.for('unchanged') && (typeof this.solutionId !== 'string' || this.solutionId.length <= 0)) return false

    return true
  }

  static forPatching (labId) {
    return new this(
      labId,
      Symbol.for('unchanged'),
      Symbol.for('unchanged'),
      Symbol.for('unchanged'),
      Symbol.for('unchanged'),
      Symbol.for('unchanged'),
      Symbol.for('unchanged'),
      Symbol.for('unchanged')
    )
  }
}
