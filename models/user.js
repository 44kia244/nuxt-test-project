export default class User {
  constructor (userId = null, email = null, displayName = null, photoURL = null, role = 'user') {
    this.setUserId(userId)
    this.setEmail(email)
    this.setDisplayName(displayName)
    this.setPhotoURL(photoURL)
    this.setRole(role)
    this.setPassword(Symbol.for('unchanged'))
  }

  getUserId () {
    return this.userId
  }

  getEmail () {
    return this.email
  }

  getDisplayName () {
    return this.displayName
  }

  getPhotoURL () {
    return this.photoURL
  }

  getRole () {
    return this.role
  }

  getPassword () {
    return this.password
  }

  setUserId (userId) {
    this.userId = userId
  }

  setEmail (email) {
    this.email = email
  }

  setDisplayName (displayName) {
    this.displayName = displayName
  }

  setPhotoURL (photoURL) {
    this.photoURL = photoURL
  }

  setRole (role) {
    this.role = role
  }

  setPassword (password) {
    this.password = password
  }

  toJSON () {
    return {
      userId: this.getUserId(),
      email: this.getEmail(),
      displayName: this.getDisplayName(),
      photoURL: this.getPhotoURL(),
      role: this.getRole()
    }
  }

  validate () {
    let emailRegex = /^(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])$/

    if (typeof this.userId !== 'string') {
      console.log('userId')
      return false
    }
    if (this.email !== Symbol.for('unchanged') && (typeof this.email !== 'string' || this.email.length < 1 || !this.email.match(emailRegex))) {
      console.log('email')
      return false
    }
    if (this.password !== Symbol.for('unchanged') && (typeof this.password !== 'string' || this.password.length < 8)) {
      console.log('password')
      return false
    }
    if (this.displayName !== Symbol.for('unchanged') && (typeof this.displayName !== 'string' || this.displayName.length < 1)) {
      console.log('displayName')
      return false
    }
    if (this.photoURL !== Symbol.for('unchanged') && (typeof this.photoURL !== 'string' || (this.photoURL !== '' && !(this.photoURL.startsWith('http://') || this.photoURL.startsWith('https://'))))) {
      console.log('photoURL')
      return false
    }
    if (this.role !== Symbol.for('unchanged') && (typeof this.role !== 'string' || !['user', 'admin'].includes(this.role))) {
      console.log('role')
      return false
    }

    return true
  }

  static forPatching (userId) {
    return new this(
      userId,
      Symbol.for('unchanged'),
      Symbol.for('unchanged'),
      Symbol.for('unchanged'),
      Symbol.for('unchanged')
    )
  }
}
