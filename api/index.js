import UsersApi from './users'
import LabsApi from './labs'

const express = require('express')
const app = express()

app.get('/', (req, res) => {
    res.status(200).json(null)
})

app.use('/users', UsersApi)
app.use('/labs', LabsApi)

export default app