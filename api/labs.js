import Lab from '../models/lab'
import LabService from '../service/LabService'
import ErrorProxy from '../helpers/error-proxy'

const express = require('express')
const app = express()
const bodyParser = require('body-parser')

app.use(bodyParser.json())

app.get('/', ErrorProxy(listLabs))
app.get('/:labId', ErrorProxy(getLab))
app.post('/', ErrorProxy(createLab))
app.put('/:labId', ErrorProxy(replaceLabInfo))
app.patch('/:labId', ErrorProxy(updateLabInfo))
app.delete('/:labId', ErrorProxy(deleteLab))

async function listLabs (data, context) {
  if (!context.auth.uid) {
    throw new ConditionalError('Unauthenticated')
  }

  if (context.auth.token.role !== 'admin') {
    throw new ConditionalError('Cannot list labs without administrative privilege')
  }

  // For larger number of entities, pagination needed
  return await LabService.findAll()
}

async function getLab (data, context) {
  if (!context.auth.uid) {
    throw new ConditionalError('Unauthenticated')
  }

  if (context.auth.token.role !== 'admin') {
    throw new ConditionalError('Cannot get lab without administrative privilege')
  }

  return await LabService.findOneById(context.params.labId)
}

async function createLab (data, context) {
  if (!context.auth.uid) {
    throw new ConditionalError('Unauthenticated')
  }

  if (context.auth.token.role !== 'admin') {
    throw new ConditionalError('Cannot create lab without administrative privilege')
  }

  let lab = new Lab(
    '',
    data.labName,
    data.tags,
    data.vmImage,
    data.instructions,
    data.score,
    data.hint,
    data.solutionId
  )

  let labId = await LabService.create(lab)

  return { labId }
}

async function replaceLabInfo (data, context) {
  if (!context.auth.uid) {
    throw new ConditionalError('Unauthenticated')
  }

  if (context.auth.token.role !== 'admin') {
    throw new ConditionalError('Cannot modify lab without administrative privilege')
  }

  let labId = context.params.labId

  let lab = new Lab(
    labId,
    data.labName,
    data.tags,
    data.vmImage,
    data.instructions,
    data.score,
    data.hint,
    data.solutionId
  )

  await LabService.save(lab)

  return null
}

async function updateLabInfo (data, context) {
  if (!context.auth.uid) {
    throw new ConditionalError('Unauthenticated')
  }

  if (context.auth.token.role !== 'admin') {
    throw new ConditionalError('Cannot modify lab without administrative privilege')
  }

  let labId = context.params.labId
  let lab = Lab.forPatching(labId)

  for (let patchKey in data) {
    switch (patchKey) {
      case 'labName': {
        lab.setLabName(data[patchKey])
        break
      }
      case 'tags': {
        lab.setTags(data[patchKey])
        break
      }
      case 'vmImage': {
        lab.setVmImage(data[patchKey])
        break
      }
      case 'instructions': {
        lab.setInstructions(data[patchKey])
        break
      }
      case 'score': {
        lab.setScore(data[patchKey])
        break
      }
      case 'hint': {
        lab.setHint(data[patchKey])
        break
      }
      case 'solutionId': {
        lab.setSolutionId(data[patchKey])
        break
      }
    }
  }

  await LabService.save(lab)

  return null
}

async function deleteLab (data, context) {
  if (!context.auth.uid) {
    throw new ConditionalError('Unauthenticated')
  }

  if (context.auth.token.role !== 'admin') {
    throw new ConditionalError('Cannot delete lab without administrative privilege')
  }

  await LabService.deleteById(context.params.labId)
  return null
}

export default app
