import User from '../models/user'
import UserService from '../service/UserService'
import ErrorProxy, { ConditionalError } from '../helpers/error-proxy'

const express = require('express')
const app = express()
const bodyParser = require('body-parser')

app.use(bodyParser.json())

app.get('/', ErrorProxy(listUsers))
app.get('/:userId', ErrorProxy(getUser))
app.post('/', ErrorProxy(registerUser))
app.put('/:userId', ErrorProxy(replaceUserInfo))
app.patch('/:userId', ErrorProxy(updateUserInfo))
app.delete('/:userId', ErrorProxy(deleteUser))

async function listUsers (data, context) {
  if (!context.auth.uid) {
    throw new ConditionalError('Unauthenticated')
  }

  if (context.auth.token.role !== 'admin') {
    throw new ConditionalError('Cannot list users without administrative privilege')
  }

  // For larger number of entities, pagination needed
  return await UserService.findAll()
}

async function getUser (data, context) {
  let userId = context.params.userId

  if (!context.auth.uid) {
    throw new ConditionalError('Unauthenticated')
  }

  if (context.auth.token.role !== 'admin' && userId !== context.auth.uid) {
    throw new ConditionalError('Cannot get non-own user without administrative privilege')
  }

  return await UserService.findOneById(userId)
}

async function registerUser (data, context) {
  let role = 'user'

  // Only admin can specify role for a user
  if (context.auth.uid && context.auth.token.role === 'admin' && data.role) role = data.role

  let user = new User('', data.email, data.displayName, data.photoURL, role)
  user.setPassword(data.password)

  let userId = await UserService.create(user)

  return { userId }
}

async function replaceUserInfo (data, context) {
  let userId = context.params.userId
  let user = new User(userId, data.email, data.displayName, data.photoURL, data.role)

  if (!context.auth.uid) {
    throw new ConditionalError('Unauthenticated')
  }

  if (context.auth.token.role !== 'admin' && user.getUserId() !== context.auth.uid) {
    throw new ConditionalError('Cannot modify non-own user without administrative privilege')
  }

  // Password can be changed
  if (data.password) user.setPassword(data.password)

  await UserService.save(user)

  return null
}

async function updateUserInfo (data, context) {
  let userId = context.params.userId
  let user = User.forPatching(userId)

  if (!context.auth.uid) {
    throw new ConditionalError('Unauthenticated')
  }

  if (context.auth.token.role !== 'admin' && user.getUserId() !== context.auth.uid) {
    throw new ConditionalError('Cannot modify non-own user without administrative privilege')
  }

  for (let patchKey in data) {
    switch (patchKey) {
      case 'email': {
        user.setEmail(data[patchKey])
        break
      }
      case 'displayName': {
        user.setDisplayName(data[patchKey])
        break
      }
      case 'photoURL': {
        user.setPhotoURL(data[patchKey])
        break
      }
      case 'role': {
        user.setRole(data[patchKey])
        break
      }
      case 'password': {
        user.setPassword(data[patchKey])
        break
      }
    }
  }

  await UserService.save(user)

  return null
}

async function deleteUser (data, context) {
  if (!context.auth.uid) {
    throw new ConditionalError('Unauthenticated')
  }

  if (context.auth.token.role !== 'admin') {
    throw new ConditionalError('Cannot delete user without administrative privilege')
  }

  await UserService.deleteById(context.params.userId)
  return null
}

export default app
