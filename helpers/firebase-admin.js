import config from '../app.config'

const exportValues = {}

Object.defineProperty(exportValues, 'admin', {
    enumerable: true,
    configurable: true,
    get () {
        const admin = require('firebase-admin')

        const findAdminApp = admin.apps.find(e => e.name_ === 'admin')

        if (!findAdminApp) {
            let adminApp = admin.initializeApp({
                ...config.firebaseAdmin,
                credential: admin.credential.cert(config.serviceAccount)
            }, 'admin')

            Object.defineProperty(exportValues, 'admin', {
                enumerable: true,
                configurable: true,
                value: adminApp
            })

            return adminApp
        }

        return findAdminApp
    }
})

export default exportValues
