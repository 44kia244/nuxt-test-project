import config from '../app.config'

const databaseValues = {}

Object.defineProperty(databaseValues, 'pool', {
    enumerable: true,
    configurable: true,
    get () {
        const { Pool } = require('pg')

        Object.defineProperty(databaseValues, 'pool', {
            enumerable: true,
            configurable: true,
            value: new Pool(config.database)
        })

        return databaseValues.pool
    }
})

export default databaseValues
