import FirebaseAdmin from './firebase-admin'

class ConditionalError extends Error {
}

function errorProxy (runner = function(data, context) {}) {
    return async function (req, res) {
        try {
            let idToken = (req.headers['authorization'] || '').split(' ')[1] || null
            let auth = await getUser(idToken)
            let context = { auth, params: req.params }

            let result = runner(req.body, context)
            if (result instanceof Promise) result = await result

            res.status(200).send(result)
        } catch (e) {
            if (e instanceof ConditionalError) {
                res.status(400).send({
                    error: e.message
                })
            } else {
                console.error(e)
                res.status(500).send({
                    error: 'Internal Server Error'
                })
            }
        }
    }
}

async function getUser (idToken) {
    if (!idToken) return {
        uid: null,
        token: null
    }

    let decodedToken = await FirebaseAdmin.admin.auth().verifyIdToken(idToken)

    return {
        uid: decodedToken.uid,
        token: decodedToken
    }
}

export { ConditionalError }
export default errorProxy
