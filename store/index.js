export const state = () => ({
})

export const mutations = {
}

export const actions = {
    nuxtServerInit (store, { res }) {
        if (res && res.locals && res.locals.user) {
          const { allClaims: claims, ...authUser } = res.locals.user
          store.commit('auth/setCurrentUserSSR', { claims, authUser })
        }
      }
}

export const getters = {
}

