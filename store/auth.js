export const state = () => ({
  currentUser: null
})

export const mutations = {
  setCurrentUser (state, { authUser, claims }) {
    if (authUser) {
      state.currentUser = { 
        uid: authUser.uid,
        email: authUser.email,
        displayName: authUser.displayName,
        photoURL: authUser.photoURL,
        role: claims.role || 'user',
        idToken: ''
      }
    } else {
      state.currentUser = null
    }

    if (authUser) console.log('now logged in')
    else console.log('now logged out')
  },
  setCurrentUserSSR (state, { authUser, claims }) {
    if (authUser) {
      state.currentUser = { 
        uid: authUser.uid,
        email: authUser.email,
        displayName: authUser.displayName,
        photoURL: authUser.photoURL,
        role: claims.role || 'user',
        idToken: authUser.idToken
      }
    } else {
      state.currentUser = null
    }

    

    if (authUser) console.log('now logged in')
    else console.log('now logged out')
  }
}

export const actions = {
  linkWithAxios (store, { axios, $fireAuth }) {
    axios.interceptors.request.use(async config => {
      if (store.getters.isAuthenticated) {
        let idToken

        if (store.state.currentUser.idToken) {
          idToken = store.state.currentUser.idToken
        } else {
          idToken = await $fireAuth.currentUser.getIdToken()
        }
        
        config.headers.Authorization = `Bearer ${idToken}`
      }

      return config
    })
  }
}

export const getters = {
  isAuthenticated (state) {
    return !!state.currentUser
  }
}
