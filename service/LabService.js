import Lab from "../models/lab"
import Database from '../helpers/database'
import PushId from '../helpers/push-id'
import { ConditionalError } from "../helpers/error-proxy"

export default class LabService {
  static tableName = 'Labs'
  static tableKey = 'labId'

  static async findAll () {
    let result = await Database.pool.query({
      name: 'fetch-lab-all',
      text: `SELECT "labId", "labName", "tags", "vmImage", "instructions", "score", "hint", "solutionId" from public."${this.tableName}"`,
      values: []
    })

    let rows = []

    for (let i = 0; i < result.rows.length; i++) {
      rows.push(new Lab(
        result.rows[i].labId,
        result.rows[i].labName,
        result.rows[i].tags,
        result.rows[i].vmImage,
        result.rows[i].instructions,
        result.rows[i].score,
        result.rows[i].hint,
        result.rows[i].solutionId
      ))
    }

    return rows
  }

  static async findOneById (id) {
    let result = await Database.pool.query({
      name: 'fetch-lab',
      text: `SELECT "labId", "labName", "tags", "vmImage", "instructions", "score", "hint", "solutionId" from public."${this.tableName}" WHERE "${this.tableKey}" = $1`,
      values: [id]
    })

    if (result.rows.length === 1) {
      return new Lab(
        result.rows[i].labId,
        result.rows[i].labName,
        result.rows[i].tags,
        result.rows[i].vmImage,
        result.rows[i].instructions,
        result.rows[i].score,
        result.rows[i].hint,
        result.rows[i].solutionId
      )
    } else {
      return null
    }
  }

  static async create (lab) {
    if (!(lab instanceof Lab)) return

    if (!lab.validate()) throw new ConditionalError('Form validation failed')

    let labId = PushId.generate()

    await Database.pool.query({
      name: 'create-lab',
      text: `INSERT INTO public."${this.tableName}" ("${this.tableKey}", "labName", "tags", "vmImage", "instructions", "score", "hint", "solutionId") VALUES ($1, $2, $3, $4, $5, $6, $7, $8)`,
      values: [labId, lab.getLabName(), lab.getTags(), lab.getVmImage(), lab.getInstructions(), lab.getScore(), lab.getHint(), lab.getSolutionId()]
    })

    return labId
  }

  static async save (lab) {
    if (!(lab instanceof Lab)) return

    if (!lab.validate()) throw new ConditionalError('Form validation failed')

    let unchanged = Symbol.for('unchanged')

    let query = {
      name: 'upd-lab-',
      text: `UPDATE public."${this.tableName}" SET `,
      values: []
    }

    let changes = []

    if (lab.getLabName() !== unchanged) changes.push(['labName', lab.getLabName()])
    if (lab.getTags() !== unchanged) changes.push(['tags', lab.getTags()])
    if (lab.getVmImage() !== unchanged) changes.push(['vmImage', lab.getVmImage()])
    if (lab.getInstructions() !== unchanged) changes.push(['instructions', lab.getInstructions()])
    if (lab.getScore() !== unchanged) changes.push(['score', lab.getScore()])
    if (lab.getHint() !== unchanged) changes.push(['hint', lab.getHint()])
    if (lab.getSolutionId() !== unchanged) changes.push(['solutionId', lab.getSolutionId()])

    if (changes.length) {
      query.name += changes.map(change => change[0]).join('-')
      query.text += changes.map((change, index) => `"${change[0]}" = $${index + 1}`).join(', ')
      query.values = changes.map(change => change[1])

      query.text += ` WHERE "${this.tableKey}" = $${changes.length + 1}`
      query.values.push(lab.getLabId())
    }

    await Database.pool.query(query)
  }

  static async deleteById (id) {
    await Database.pool.query({
      name: 'delete-lab',
      text: `DELETE FROM public."${this.tableName}" WHERE "${this.tableKey}" = $1`,
      values: [id]
    })
  }
}
