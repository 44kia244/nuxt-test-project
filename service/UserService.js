import User from '../models/user'
import Database from '../helpers/database'
import FirebaseAdmin from '../helpers/firebase-admin'
import { ConditionalError } from '../helpers/error-proxy'

export default class UserService {
  static tableName = 'Users'
  static tableKey = 'userId'

  static async findAll () {
    let result = await Database.pool.query({
      name: 'fetch-user-all',
      text: `SELECT "userId", "email", "displayName", "photoURL", "role" from public."${this.tableName}"`,
      values: []
    })

    let rows = []

    for (let i = 0; i < result.rows.length; i++) {
      rows.push(new User(
        result.rows[i].userId,
        result.rows[i].email,
        result.rows[i].displayName,
        result.rows[i].photoURL,
        result.rows[i].role
      ))
    }

    return rows
  }

  static async findOneById (id) {
    let result = await Database.pool.query({
      name: 'fetch-user',
      text: `SELECT "userId", "email", "displayName", "photoURL", "role" from public."${this.tableName}" WHERE "${this.tableKey}" = $1`,
      values: [id]
    })

    if (result.rows.length === 1) {
      return new User(
        result.rows[0].userId,
        result.rows[0].email,
        result.rows[0].displayName,
        result.rows[0].photoURL,
        result.rows[0].role
      )
    } else {
      return null
    }
  }

  static async create (user) {
    if (!(user instanceof User)) return

    if (!user.validate()) throw new ConditionalError('Form validation failed')

    let firebaseUser = {
      email: user.getEmail(),
      displayName: user.getDisplayName(),
      password: user.getPassword(),
      disabled: false
    }

    if (user.getPhotoURL()) firebaseUser.photoURL = user.getPhotoURL()

    let userRecord = await FirebaseAdmin.admin.auth().createUser(firebaseUser)

    await FirebaseAdmin.admin.auth().setCustomUserClaims(userRecord.uid, { role: user.getRole() })

    await Database.pool.query({
      name: 'create-user',
      text: `INSERT INTO public."${this.tableName}" ("${this.tableKey}", "email", "displayName", "photoURL", "role") VALUES ($1, $2, $3, $4, $5)`,
      values: [userRecord.uid, user.getEmail(), user.getDisplayName(), user.getPhotoURL(), user.getRole()]
    })

    return userRecord.uid
  }

  static async save (user) {
    if (!(user instanceof User)) return

    if (!user.validate()) throw new ConditionalError('Form validation failed')

    let unchanged = Symbol.for('unchanged')

    let query = {
      name: 'update-user-',
      text: `UPDATE public."${this.tableName}" SET `,
      values: []
    }

    let firebaseUserUpdate = {}
    let firebaseUserClaims = null

    let changes = []

    if (user.getEmail() !== unchanged) {
      changes.push(['email', user.getEmail()])
      firebaseUserUpdate.email = user.getEmail()
    }

    if (user.getDisplayName() !== unchanged) {
      changes.push(['displayName', user.getDisplayName()])
      firebaseUserUpdate.displayName = user.getDisplayName()
    }

    if (user.getPhotoURL() !== unchanged) {
      changes.push(['photoURL', user.getPhotoURL()])
      firebaseUserUpdate.photoURL = user.getPhotoURL() || null
    }

    if (user.getRole() !== unchanged) {
      changes.push(['role', user.getRole()])
      if (!firebaseUserClaims) firebaseUserClaims = {}
      firebaseUserClaims.role = user.getRole()
    }

    if (user.getPassword() !== unchanged) {
      firebaseUserUpdate.password = user.getPassword()
    }

    if (changes.length) {
      query.name += changes.map(change => change[0]).join('-')
      query.text += changes.map((change, index) => `"${change[0]}" = $${index + 1}`).join(', ')
      query.values = changes.map(change => change[1])

      query.text += ` WHERE "${this.tableKey}" = $${changes.length + 1}`
      query.values.push(user.getUserId())
    }

    await FirebaseAdmin.admin.auth().updateUser(user.getUserId(), firebaseUserUpdate)
    if (firebaseUserClaims) await FirebaseAdmin.admin.auth().setCustomUserClaims(user.getUserId(), firebaseUserClaims)
    await Database.pool.query(query)
  }

  static async deleteById (id) {
    await FirebaseAdmin.admin.auth().deleteUser(id)
    await Database.pool.query({
      name: 'delete-user',
      text: `DELETE FROM public."${this.tableName}" WHERE "${this.tableKey}" = $1`,
      values: [id]
    })
  }
}
