import config from '../app.config'
import Database from '../helpers/database'

let initQueries = [
  // Create Table "Labs"
  `CREATE TABLE public."Labs"
  (
      "labId" character varying(20) COLLATE pg_catalog."default" NOT NULL,
      "labName" character varying(100) COLLATE pg_catalog."default" NOT NULL,
      tags character varying(30)[] COLLATE pg_catalog."default" NOT NULL,
      "vmImage" character varying(150) COLLATE pg_catalog."default" NOT NULL,
      instructions text COLLATE pg_catalog."default" NOT NULL,
      score integer NOT NULL,
      hint text COLLATE pg_catalog."default" NOT NULL,
      "solutionId" character varying(20) COLLATE pg_catalog."default" NOT NULL,
      CONSTRAINT "Labs_pkey" PRIMARY KEY ("labId")
  ) TABLESPACE pg_default;`,

  // Create Table "Users"
  `CREATE TABLE public."Users"
  (
      "userId" character varying(50) COLLATE pg_catalog."default" NOT NULL,
      email character varying(100) COLLATE pg_catalog."default" NOT NULL,
      "displayName" character varying(200) COLLATE pg_catalog."default" NOT NULL,
      "photoURL" character varying(500) COLLATE pg_catalog."default",
      role character varying(10) COLLATE pg_catalog."default",
      CONSTRAINT "Users_pkey" PRIMARY KEY ("userId"),
      CONSTRAINT "Users_email_key" UNIQUE (email)
  ) TABLESPACE pg_default;`,

  // Chown Table to self
  `ALTER TABLE public."Labs"
    OWNER to ${config.database.user};`,

  // Chown Table to self
  `ALTER TABLE public."Users"
    OWNER to ${config.database.user};`
]

async function main () {
  for (let i = 0; i < initQueries.length; i++) {
    let query = initQueries[i]

    await Database.pool.query(query)
  }
}

main().then(() => {
  Database.pool.end()
  process.exit(0)
})
