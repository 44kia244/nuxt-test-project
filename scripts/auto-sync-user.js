import FirebaseAdmin from '../helpers/firebase-admin'
import Database from '../helpers/database'
import User from '../models/user'

async function main () {
  // TODO: nextPageToken not implemented
  let nextPageToken

  let result = await FirebaseAdmin.admin.auth().listUsers(1000, nextPageToken)

  for (let i = 0; i < result.users.length; i++) {
    let userRecord = result.users[i]
    let user = new User(userRecord.uid, userRecord.email, userRecord.displayName || '', userRecord.photoURL || '', (userRecord.customClaims && userRecord.customClaims.role) || 'user')

    console.log(`processing ${user.getEmail()} ...`)

    try {
      await Database.pool.query({
        name: 'create-user',
        text: `INSERT INTO public."Users" ("userId", "email", "displayName", "photoURL", "role") VALUES ($1, $2, $3, $4, $5)`,
        values: [userRecord.uid, user.getEmail(), user.getDisplayName(), user.getPhotoURL(), user.getRole()]
      })
    } catch (e) {
      console.warn(`[WARN] failed to add user: ${user.getEmail()}`)
      console.warn(`       the error was: ${e.message}`)
    }
  }
}

main().then(() => {
  Database.pool.end()
  process.exit(0)
})
