import FirebaseAdmin from '../helpers/firebase-admin'
import User from '../models/user'
import UserService from '../service/UserService'

async function main () {
  let emailRegex = /^(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])$/
  let email = process.argv[2]

  if (typeof email !== 'string' || !email.match(emailRegex)) {
    console.error('Invalid email: ', email)
    return 1
  }

  try {
    let userRecord = await FirebaseAdmin.admin.auth().getUserByEmail(email)
    let userId = userRecord.uid

    let user = User.forPatching(userId)

    user.setRole('admin')

    await UserService.save(user)
  } catch (e) {
    console.warn('User not found or something went wrong')
    return 1
  }
}

main().then(r => {
  process.exit(r || 0)
})
